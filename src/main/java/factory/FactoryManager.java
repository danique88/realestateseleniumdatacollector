package factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FactoryManager implements BaseSeleniumTest {

  public static WebDriver webDriver;

  public FactoryManager() {
    System.setProperty("webdriver.chrome.driver", CHROME_DRIVER);
    webDriver = new ChromeDriver();
  }

  public static void login(String url) {
    webDriver.get(url);
  }

  public static void click(String xpath) {
    webDriver.findElement(By.xpath(xpath)).click();
  }

  public static void type(String xpath, String value) {
    webDriver.findElement(By.xpath(xpath)).sendKeys(value);
  }

  public static void select(String xpath, String value) {
    Select dropdown = new Select(webDriver.findElement(By.xpath(xpath)));
    dropdown.selectByValue(value);
  }

  public void timeOut(Long ms) throws Exception {
    try {
      Thread.sleep(ms);
    }
    catch (InterruptedException e) {
      throw new Exception("Thread Failed Is it possible", e);
    }
  }

  public String getText(String xpath) {
    return webDriver.findElement(By.xpath(xpath)).getText();
  }

  public boolean isElementVisible(String xpath) {
    try {
      return webDriver.findElement(By.xpath(xpath)).isDisplayed();
    }
    catch (Exception e) {
      return false;
    }

  }


}
