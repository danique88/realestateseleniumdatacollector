package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public interface BaseSeleniumTest {

    public static final String CHROME_DRIVER = "C:\\Users\\Danica.Cvetanovic\\personalProjects\\RealEstateSeleniumDataCollector\\drivers\\chromedriver.exe";

    public static WebDriver webDriver = new ChromeDriver();


}
