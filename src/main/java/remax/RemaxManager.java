package remax;

import dto.RealEstateProperty;
import enums.PropertyType;
import factory.FactoryManager;

import javax.mail.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.NoSuchElementException;


/**
 * The type Remax manager.
 */
public class RemaxManager implements RemaxConstants {

  private static FactoryManager factory;
  private StringBuilder sb;

  /**
   * Instantiates a new Remax manager.
   */
  public RemaxManager() {
    factory = new FactoryManager();
    sb = new StringBuilder();
  }

  /**
   * Login remax manager.
   *
   * @return the remax manager
   */
  public RemaxManager login() {
    factory.login(URL);
    return this;
  }

  /**
   * Select buy remax manager.
   *
   * @return the remax manager
   */
  public RemaxManager selectBuy() {
    factory.click(BUY_XPATH);
    return this;
  }

  /**
   * Type city remax manager.
   *
   * @param city the city
   * @return the remax manager
   * @throws Exception the exception
   */
  public RemaxManager typeCity(String city) throws Exception {
    factory.type(CITY_XPATH, city);
    try {
      factory.timeOut((long) 2000);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    factory.click(CITY_DROPDOWN_XPATH);
    return this;
  }

  /**
   * Select property type remax manager.
   *
   * @param propertyType the property type
   * @return the remax manager
   */
  public RemaxManager selectPropertyType(String propertyType) {
    factory.click(propertyType);
    return this;
  }

  /**
   * Search remax manager.
   *
   * @return the remax manager
   */
  public RemaxManager search() {
    factory.click(SEARCH_XPATH);
    return this;
  }

  /**
   * Gets name of result page.
   *
   * @return the name of result page
   */
  public String getNameOfResultPage() {
    return factory.getText(RESULT_PAGE_XPATH);
  }

  /**
   * Click next page remax manager.
   *
   * @param page the page
   * @return the remax manager
   * @throws InterruptedException the interrupted exception
   */
  public RemaxManager clickNextPage(int page) throws InterruptedException {
    String nextPage = String.format(NEXT_PAGE_XPATH, page);

    try {
      Thread.sleep(1000);
      factory.click(nextPage);
    }
    catch (NoSuchElementException e) {
      Logger.getLogger(RemaxManager.class.getName()).log(Level.INFO, "You have reached the last page!");
    }

    return this;
  }

  /**
   * Collect data array list.
   *
   * @param propertyType the property type
   * @return the array list
   * @throws InterruptedException the interrupted exception
   */
  public ArrayList<RealEstateProperty> collectData(PropertyType propertyType) throws InterruptedException {
    ArrayList<RealEstateProperty> results = new ArrayList<RealEstateProperty>();

    // go through all pages
    int page = 3;

    while (factory.isElementVisible(String.format(CURRENT_PAGE_XPATH, page))) {
      // there are max 24 properties on page
      for (int i = 1; i <= NUMBER_OF_PROPERTIES_ON_PAGE; i++) {
        RealEstateProperty realEstateProperty = new RealEstateProperty();
        try {
          String[] cityAndArea = factory.getText(String.format(CITY_AREA_XPATH, i)).split(",");
          String city = cityAndArea[1];
          String area = cityAndArea[0];
          realEstateProperty.setCity(city);
          realEstateProperty.setPropertyType(propertyType);

          realEstateProperty.setArea(area);
          realEstateProperty.setPrice(getPrice(i));
          realEstateProperty.setNumberOfBedrooms(
            Integer.valueOf(factory.getText(String.format(NUMBER_OF_BEDROOMS_XPATH, i))));
          String squareMeters = factory.getText(String.format(SQUARE_METERS_XPATH, i));
          squareMeters = (squareMeters.equals("")) ? "0" : squareMeters;
          realEstateProperty.setSquareMeters(Integer.valueOf(squareMeters));

          results.add(realEstateProperty);

        }
        catch (Exception e) {
          try {
            String[] cityAndArea = factory.getText(String.format(CITY_AREA_XPATH2, i)).split(",");
            String area = cityAndArea[0];
            String city = cityAndArea[1];
            realEstateProperty.setArea(area);
            realEstateProperty.setCity(city);
            realEstateProperty.setPropertyType(propertyType);
            realEstateProperty.setPrice(getPrice2(i));
            realEstateProperty.setNumberOfBedrooms(
              Integer.valueOf(factory.getText(String.format(NUMBER_OF_BEDROOMS_XPATH2, i))));
            String squareMeters = factory.getText(String.format(SQUARE_METERS_XPATH2, i));
            squareMeters = (squareMeters.equals("")) ? "0" : squareMeters;
            realEstateProperty.setSquareMeters(Integer.valueOf(squareMeters));

            results.add(realEstateProperty);
          }
          catch (Exception e2) {
            Logger.getLogger(RealEstateProperty.class.getName()).log(Level.INFO, e2.getMessage());
          }

        }

      }

      page++;
      clickNextPage(page);
    }

    return results;
  }

  /**
   * Sets search criteria.
   *
   * @param propertyType the property type
   * @return the search criteria
   * @throws Exception the exception
   */
  public RemaxManager setSearchCriteria(String propertyType) throws Exception {
    login()
      .selectBuy()
      .selectPropertyType(propertyType)
      .search();

    return this;
  }

  /**
   * Export.
   *
   * @param realEstateProperties the real estate properties
   * @throws IOException the io exception
   * @throws MessagingException the messaging exception
   * @throws MessagingException the messaging exception
   */
  public void export(ArrayList<RealEstateProperty> realEstateProperties)
    throws IOException, MessagingException, javax.mail.MessagingException {
    BufferedWriter br = new BufferedWriter(new FileWriter(RESULT_DIR));
    getRemaxHeader();
    for (RealEstateProperty element : realEstateProperties) {
      getRealEstateCSVRow(element);
    }

    br.write(sb.toString());
    br.close();


    sendEmail();


  }

  /*PRIVATE METHODS*/

  private void sendEmail() throws MessagingException {
    // sets SMTP server properties
//        final String userName = "cvetanovicdanica88@gmail.com";
//        final String password = "b4etcvsy";
//        final String toAddress = "cvetanovicc@gmail.com";
//        String subject = "Test email from Danica";
//        String message = "Proveravamo da li radi automatsko slanje mejl sa rezultatima";
//
//        Properties properties = new Properties();
//        properties.put("mail.smtp.host", "smtp.gmail.com");
//        properties.put("mail.smtp.port", "587");
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.user", userName);
//        properties.put("mail.password", password);
//
//        // creates a new session with an authenticator
//        Authenticator auth = new Authenticator() {
//            public PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(userName, password);
//            }
//        };
//        Session session = Session.getInstance(properties, auth);
//
//        // creates a new e-mail message
//        Message msg = new MimeMessage(session);
//
//        msg.setFrom(new InternetAddress(userName));
//        InternetAddress[] toAddresses = {new InternetAddress(toAddress), new InternetAddress(userName)};
//        msg.setRecipients(Message.RecipientType.TO, toAddresses);
//        msg.setSubject(subject);
//        msg.setSentDate(new Date());
//
//        // creates message part
//        MimeBodyPart messageBodyPart = new MimeBodyPart();
//        messageBodyPart.setContent(message, "text/html");
//
//        // creates multi-part
//        Multipart multipart = new MimeMultipart();
//        multipart.addBodyPart(messageBodyPart);
//
//        MimeBodyPart attachPart = new MimeBodyPart();
//
//        try {
//            attachPart.attachFile("C:\\java\\RealstateDataCollector\\selenium-workshop-code-examples-master\\realestate-data-collector");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//
//        multipart.addBodyPart(attachPart);
//
//        msg.setContent(multipart);
//
//        // sends the e-mail
//        Transport.send(msg);
  }


  /*Private methods*/

  private void appendInCSV(String value) {
    sb.append(value);
    sb.append(",");
  }

  private void getRemaxHeader() {
    appendInCSV("CITY");
    appendInCSV("AREA");
    appendInCSV("PROPERTY_TYPE");
    appendInCSV("NUMBER_OF_BEDROOMS");
    appendInCSV("SQUARE_METERS");
    sb.append("PRICE");
    sb.append("\n");
  }

  private void getRealEstateCSVRow(RealEstateProperty realEstateProperty) {
    appendInCSV(realEstateProperty.getCity());
    appendInCSV(realEstateProperty.getArea());
    appendInCSV(realEstateProperty.getPropertyType().getPropertyType());
    appendInCSV(String.valueOf(realEstateProperty.getNumberOfBedrooms()));
    appendInCSV(String.valueOf(realEstateProperty.getSquareMeters()));
    sb.append(String.valueOf(realEstateProperty.getPrice()));
    sb.append("\n");
  }

  private int getPrice(int index) {
    String priceText =
      factory.getText(String.format(PRICE_XPATH, index)).trim().replace(",", "").replace(" ", "").replace("€", "");
    return (priceText.equals("UponRequest")) ? 0 : Integer.valueOf(priceText);
  }

  private int getPrice2(int index) {
    String priceText =
      factory.getText(String.format(PRICE_XPATH2, index)).trim().replace(",", "").replace(" ", "").replace("€", "");
    return (priceText.equals("UponRequest")) ? 0 : Integer.valueOf(priceText);
  }

}
