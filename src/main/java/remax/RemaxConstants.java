package remax;

public interface RemaxConstants {

  public static final String URL = "https://www.remax.com.cy";
  public static final String RESULT_DIR =
    "results\\RealEstateRemaxData.csv";

  // CITIES
  public static final String LIMASSOL = "Limassol";

  // WEBSITE DATA
  public static final int NUMBER_OF_PROPERTIES_ON_PAGE = 24;
  public static final String RESULT_PAGE_HOUSE_NAME = "Limassol, Cyprus Real Estate & House For Sale";
  public static final String RESULT_PAGE_APARTMENT_NAME = "Limassol, Cyprus Real Estate & Apartment For Sale";

  //XPATHS
  public static final String NEXT_PAGE_XPATH = "//ul[@class='pagination']/li[%d]/a";
  public static final String CURRENT_PAGE_XPATH = "//ul[@class='pagination']/li[%d]";
  public static final String CITY_XPATH = "//*[@id='geolocctrl']";
  public static final String CITY_DROPDOWN_XPATH = "//li[@class='ui-menu-item']/a";
  public static final String BUY_XPATH = "//*[@id='qs-simple']/div/div[1]/div/label[1]";
  public static final String PROPERTY_HOUSE_XPATH =
    "//*[@id='ctl01_ctl00_ddlCombinedPropertyTypes']//optgroup[1]/option[2]";
  public static final String PROPERTY_APARTMENT_XPATH =
    "//*[@id='ctl01_ctl00_ddlCombinedPropertyTypes']//optgroup[1]/option[1]";
  public static final String PROPERTY_WHOLE_BUILDING_XPATH =
    "//*[@id='ctl01_ctl00_ddlCombinedPropertyTypes']//optgroup[1]/option[3]";
  public static final String SEARCH_XPATH = "//*[@id='btnDummy2']";
  public static final String RESULT_PAGE_XPATH = "//*[@id='h1-title-tag']";
  public static final String CITY_AREA_XPATH = "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[5]/a";
  public static final String CITY_AREA_XPATH2 = "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[4]/a";
  public static final String PRICE_XPATH = "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[7]/span/a";
  public static final String PRICE_XPATH2 = "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[6]/span/a";
  public static final String NUMBER_OF_BEDROOMS_XPATH =
    "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[9]/span[2]";
  public static final String NUMBER_OF_BEDROOMS_XPATH2 =
    "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[8]/span[2]";
  public static final String SQUARE_METERS_XPATH =
    "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[9]/span[4]";
  public static final String SQUARE_METERS_XPATH2 =
    "//div[@class='gallery-container exclusive-alt']/div/div[%d]/div/div[8]/span[4]";

}
