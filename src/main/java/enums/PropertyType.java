package enums;

public enum PropertyType {
    APARTMENT("Apartment"),
    HOUSE("House"),
    WHOLE_APARTMENT_BUILDING("Whole Apartment Building"),
    PLOT("Plot");

    private String propertyType;
    PropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyType() {
        return this.propertyType;
    }
}
