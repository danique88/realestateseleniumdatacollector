package remax;

import dto.RealEstateProperty;
import enums.PropertyType;
import factory.BaseSeleniumTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestRemax implements BaseSeleniumTest, RemaxConstants {

    RemaxManager remaxManager = new RemaxManager();


    @Test
    public void getAllFromRemax() throws Exception {
        // HOUSE
        remaxManager.setSearchCriteria(PROPERTY_HOUSE_XPATH);

        ArrayList<RealEstateProperty> results = remaxManager.collectData(PropertyType.HOUSE);

        //APARTMENT
        remaxManager.setSearchCriteria(PROPERTY_APARTMENT_XPATH);

        results.addAll(remaxManager.collectData(PropertyType.APARTMENT));

        //EXPORT
        remaxManager.export(results);

    }


    @After
    public void end() {
        webDriver.quit();
    }


}
